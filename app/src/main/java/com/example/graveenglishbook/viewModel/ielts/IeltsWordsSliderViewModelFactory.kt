package com.example.graveenglishbook.viewModel.ielts

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.graveenglishbook.repositories.IeltsRepository

class IeltsWordsSliderViewModelFactory(
    private val repository: IeltsRepository

): ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T =
        IeltsWordsSliderViewModel(repository) as T
}