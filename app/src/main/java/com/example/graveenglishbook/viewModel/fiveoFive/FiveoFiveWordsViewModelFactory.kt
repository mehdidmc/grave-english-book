package com.example.graveenglishbook.viewModel.fiveoFive

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.graveenglishbook.repositories.FiveoFiveRepository

class FiveoFiveWordsViewModelFactory(
    private val repository: FiveoFiveRepository

): ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return FiveoFiveWordsViewModel(repository) as T
    }
}