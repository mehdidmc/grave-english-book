package com.example.graveenglishbook.view.adapters

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.graveenglishbook.R
import com.example.graveenglishbook.databinding.LessonIeltsItemsBinding
import com.example.graveenglishbook.view.activities.WordsSliderActivity

class RecyclerIeltsItemsAdapter(private val activity: Activity): RecyclerView.Adapter<RecyclerIeltsItemsAdapter.RecyclerIeltsItemsViewHolder>() {

    private val lessonNumber = activity.resources.getStringArray(R.array.lesson_titles_ielts)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerIeltsItemsViewHolder {
        val inflater = LayoutInflater.from(activity)
        val bind: LessonIeltsItemsBinding = DataBindingUtil.inflate(inflater, R.layout.lesson_ielts_items, parent, false)
        return RecyclerIeltsItemsViewHolder(bind)
    }

    override fun onBindViewHolder(holder: RecyclerIeltsItemsViewHolder, position: Int) {
        val lessonTitle = "LESSON ${lessonNumber[position]}"
        holder.bind.lessonTitleTxv.text = lessonTitle

        holder.itemView.setOnClickListener {
            val lsnNumber = position + 1
            val title = lessonNumber[position]

            val intent = Intent(activity, WordsSliderActivity::class.java)
            intent.putExtra("course", "ielts")
            intent.putExtra("lesson_number", lsnNumber)
            intent.putExtra("lesson_title", title)
            activity.startActivity(intent)
        }
    }

    override fun getItemCount(): Int = lessonNumber.size

    inner class RecyclerIeltsItemsViewHolder(val bind: LessonIeltsItemsBinding): RecyclerView.ViewHolder(bind.root)
}