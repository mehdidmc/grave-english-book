package com.example.graveenglishbook.viewModel.ielts

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.graveenglishbook.repositories.IeltsRepository
import kotlinx.coroutines.launch

class IeltsWordsSliderViewModel(
    private val repository: IeltsRepository

): ViewModel() {

    fun getIeltsWords(lessonNumber: Int) =
        repository.getIeltsWords(lessonNumber)

    fun addToFavorite(wordId: Int) = viewModelScope.launch {
        repository.addToFavorite(wordId)
    }

    fun removeFromFavorite(wordId: Int) = viewModelScope.launch {
        repository.removeFromFavorite(wordId)
    }

    suspend fun isFavorite(wordId: Int) = repository.isFavorite(wordId)

    fun clearAllFavorites() = viewModelScope.launch {
        repository.clearFavorites()
    }

    fun getIeltsFavorites() = repository.getIeltsFavoriteWords()
}