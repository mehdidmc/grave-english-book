package com.example.graveenglishbook.database.ielts

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import com.example.graveenglishbook.model.IeltsEntity

@Dao
interface IeltsDao {

    @Query("SELECT * FROM tbl_ielts WHERE lessonNumber=:lessonNumber")
    fun getIeltsWords(lessonNumber: Int): LiveData<List<IeltsEntity>>

    @Query("SELECT * FROM tbl_ielts WHERE favorite=1")
    fun getIeltsFavoriteWords(): LiveData<List<IeltsEntity>>

    @Query("UPDATE tbl_ielts SET favorite=1 WHERE id=:wordId")
    suspend fun addToFavorite(wordId: Int)

    @Query("UPDATE tbl_ielts SET favorite=0 WHERE id=:wordId")
    suspend fun removeFromFavorite(wordId: Int)

    @Query("SELECT favorite FROM tbl_ielts WHERE id=:wordId")
    suspend fun isFavorite(wordId: Int): Int

    @Query("UPDATE tbl_ielts SET favorite=0")
    suspend fun clearFavoriteWords()
}