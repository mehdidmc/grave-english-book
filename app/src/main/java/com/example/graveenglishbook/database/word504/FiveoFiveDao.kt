package com.example.graveenglishbook.database.word504

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import com.example.graveenglishbook.model.FiveoFiveEntity

@Dao
interface FiveoFiveDao {

    @Query("SELECT * FROM tbl504Words WHERE lessonNumber=:lessonNumber")
    fun getWords(lessonNumber: Int): LiveData<List<FiveoFiveEntity>>

    @Query("UPDATE tbl504Words SET favorite=1 WHERE wordID=:wordId")
    suspend fun addToFavorites(wordId: Int)

    @Query("UPDATE tbl504Words SET favorite=0 WHERE wordID=:wordId")
    suspend fun removeFromFavorite(wordId: Int)

    @Query("SELECT favorite FROM tbl504Words WHERE wordID=:wordId")
    suspend fun isFavorite(wordId: Int): Int

    @Query("UPDATE tbl504Words SET favorite=0")
    suspend fun clearFavorites()

    @Query("SELECT * FROM tbl504Words WHERE favorite=1")
    fun getFavoriteWords(): LiveData<List<FiveoFiveEntity>>

    @Query("SELECT isRead FROM tbl504Words WHERE wordID=:wordId")
    suspend fun isRead(wordId: Int): Int

    @Query("UPDATE tbl504Words SET isRead=1 WHERE wordID=:wordId")
    suspend fun setRead(wordId: Int)
}