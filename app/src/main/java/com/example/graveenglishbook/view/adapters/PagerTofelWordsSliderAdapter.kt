package com.example.graveenglishbook.view.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.viewModelScope
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.graveenglishbook.R
import com.example.graveenglishbook.databinding.WordsTofelPagerItemBinding
import com.example.graveenglishbook.model.TofelEntity
import com.example.graveenglishbook.viewModel.tofel.TofelWordsViewModel
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.launch

class PagerTofelWordsSliderAdapter(
    private val context: Context,
    private val viewModel: TofelWordsViewModel

): RecyclerView.Adapter<PagerTofelWordsSliderAdapter.PagerTofelWordsSliderViewHolder>() {

    val differ = AsyncListDiffer(this, PagerTofelWordsSliderDiffUtil())

     override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PagerTofelWordsSliderViewHolder {
         val inflater = LayoutInflater.from(parent.context)
         val bind: WordsTofelPagerItemBinding = DataBindingUtil.inflate(inflater, R.layout.words_tofel_pager_item, parent, false)
         return PagerTofelWordsSliderViewHolder(bind)
     }

     override fun onBindViewHolder(holder: PagerTofelWordsSliderViewHolder, position: Int) {
         val words = differ.currentList[position]
         holder.bind.tofelWord = words

         viewModel.viewModelScope.launch {
             if (viewModel.isFavorite(words.id) == 1) {
                 holder.bind.addFavoriteTxv.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.heart_minus,0,0,0)
                 holder.bind.addFavoriteTxv.text = context.getString(R.string.remove_Favorite)

             } else {
                 holder.bind.addFavoriteTxv.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.heart_plus,0,0,0)
                 holder.bind.addFavoriteTxv.text = context.getString(R.string.add_favorite_txv)
             }
         }

         holder.bind.addFavoriteTxv.setOnClickListener {
             if (holder.bind.addFavoriteTxv.text == context.getString(R.string.remove_Favorite)) {
                 viewModel.removeFromFavorite(words.id)
                 holder.bind.addFavoriteTxv.text = context.getString(R.string.add_favorite_txv)
                 holder.bind.addFavoriteTxv.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.heart_plus,0,0,0)
                 Snackbar.make(it, context.getString(R.string.toast_remove_favorite), Snackbar.LENGTH_SHORT).show()
             }

             else {
                 viewModel.addToFavorite(words.id)
                 holder.bind.addFavoriteTxv.text = context.getString(R.string.remove_Favorite)
                 holder.bind.addFavoriteTxv.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.heart_minus,0,0,0)
                 Snackbar.make(it, context.getString(R.string.toast_add_favorite), Snackbar.LENGTH_SHORT).show()
             }
         }
     }

     override fun getItemCount(): Int = differ.currentList.size

     inner class PagerTofelWordsSliderViewHolder(val bind: WordsTofelPagerItemBinding): RecyclerView.ViewHolder(bind.root)

     inner class PagerTofelWordsSliderDiffUtil: DiffUtil.ItemCallback<TofelEntity>() {

         override fun areItemsTheSame(oldItem: TofelEntity, newItem: TofelEntity): Boolean {
             return oldItem.id == newItem.id
         }

         override fun areContentsTheSame(oldItem: TofelEntity, newItem: TofelEntity): Boolean {
             return oldItem == newItem
         }
     }
}