package com.example.graveenglishbook.viewModel.tofel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.graveenglishbook.repositories.TofelRepository

class TofelWordsViewModelFactory(
    private val repository: TofelRepository

): ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return TofelWordsViewModel(repository) as T
    }
}