package com.example.graveenglishbook.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey

@Entity(tableName = "tbl504Words")
data class FiveoFiveEntity(
    @ColumnInfo(name = "wordID")
    @PrimaryKey(autoGenerate = true)
    var id: Int,

    @ColumnInfo(name = "word")
    var word: String,

    @ColumnInfo(name = "meaning")
    var meaning: String,

    @ColumnInfo(name = "pronun")
    var pronunce: String,

    @ColumnInfo(name = "example")
    var example: String,

    @ColumnInfo(name = "exampleMeaning")
    var meaningExample: String,

    @ColumnInfo(name = "example2")
    var example2: String,

    @ColumnInfo(name = "exampleMeaning2")
    var meaningExample2: String,

    @ColumnInfo(name = "example3")
    var example3: String,

    @ColumnInfo(name = "exampleMeaning3")
    var meaningExample3: String,

    @ColumnInfo(name = "lessonNumber")
    var lessonNumber: Int,

    @ColumnInfo(name = "isRead")
    var isRead: Int,

    @ColumnInfo(name = "favorite")
    var favorite: Int
) {
    @Ignore
    var isExpanded = false
}