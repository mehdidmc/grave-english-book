package com.example.graveenglishbook.view.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.viewModelScope
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.graveenglishbook.R
import com.example.graveenglishbook.databinding.Words504PagerItemsBinding
import com.example.graveenglishbook.model.FiveoFiveEntity
import com.example.graveenglishbook.viewModel.fiveoFive.FiveoFiveWordsViewModel
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class Pager504WordsSliderAdapter(
    private val context: Context,
    private val viewModelFiveoFive: FiveoFiveWordsViewModel

) : RecyclerView.Adapter<Pager504WordsSliderAdapter.PagerWordsSliderViewHolder>() {

    val differ = AsyncListDiffer(this, Pager504WordsSliderDiffUtil())
    private lateinit var bind: Words504PagerItemsBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PagerWordsSliderViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        bind = DataBindingUtil.inflate(inflater, R.layout.words_504_pager_items, parent, false)
        return PagerWordsSliderViewHolder(bind)
    }

    override fun onBindViewHolder(holder: PagerWordsSliderViewHolder, position: Int) {
        val currentWord = differ.currentList[position]
        holder.bind.wordEntity = currentWord

        viewModelFiveoFive.viewModelScope.launch {
            if (viewModelFiveoFive.is504Favorite(currentWord.id) == 1) {
                holder.bind.addFavoriteTxv.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.heart_minus,0,0,0)
                holder.bind.addFavoriteTxv.text = context.getString(R.string.remove_Favorite)

            } else {
                holder.bind.addFavoriteTxv.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.heart_plus,0,0,0)
                holder.bind.addFavoriteTxv.text = context.getString(R.string.add_favorite_txv)
            }
        }

        holder.bind.addFavoriteTxv.setOnClickListener {
            if (holder.bind.addFavoriteTxv.text == context.getString(R.string.remove_Favorite)) {
                viewModelFiveoFive.removeFromFavorites(currentWord.id)
                holder.bind.addFavoriteTxv.text = context.getString(R.string.add_favorite_txv)
                holder.bind.addFavoriteTxv.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.heart_plus,0,0,0)
                Snackbar.make(it, context.getString(R.string.toast_remove_favorite), Snackbar.LENGTH_SHORT).show()
            }

            else {
                viewModelFiveoFive.addTo504Favorites(currentWord.id)
                holder.bind.addFavoriteTxv.text = context.getString(R.string.remove_Favorite)
                holder.bind.addFavoriteTxv.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.heart_minus,0,0,0)
                Snackbar.make(it, context.getString(R.string.toast_add_favorite), Snackbar.LENGTH_SHORT).show()
            }
        }
    }

    override fun getItemCount(): Int {
        return differ.currentList.size
    }

    fun setReadVisibility(visibility: Int) {
        bind.isReadTxv.visibility = visibility
    }

    inner class PagerWordsSliderViewHolder(val bind: Words504PagerItemsBinding) : RecyclerView.ViewHolder(bind.root)

    inner class Pager504WordsSliderDiffUtil: DiffUtil.ItemCallback<FiveoFiveEntity>() {

        override fun areItemsTheSame(oldItem: FiveoFiveEntity, newItem: FiveoFiveEntity): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: FiveoFiveEntity, newItem: FiveoFiveEntity): Boolean {
            return oldItem == newItem
        }
    }
}