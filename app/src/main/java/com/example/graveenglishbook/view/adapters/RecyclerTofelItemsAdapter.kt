package com.example.graveenglishbook.view.adapters

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.graveenglishbook.R
import com.example.graveenglishbook.databinding.LessonsTofelItemsBinding
import com.example.graveenglishbook.view.activities.WordsSliderActivity

class RecyclerTofelItemsAdapter(
    private val activity: Activity

): RecyclerView.Adapter<RecyclerTofelItemsAdapter.RecyclerTofelItemViewHolder>() {

    private val lessonTitle = activity.resources.getStringArray(R.array.lesson_number_tofel)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerTofelItemViewHolder {
        val inflater = LayoutInflater.from(activity)
        val bind: LessonsTofelItemsBinding = DataBindingUtil.inflate(inflater, R.layout.lessons_tofel_items, parent, false)
        return RecyclerTofelItemViewHolder(bind)
    }

    override fun onBindViewHolder(holder: RecyclerTofelItemViewHolder, position: Int) {
        val lessonStrTitle = "LESSON ${lessonTitle[position]}"
        holder.bind.lessonTitleTxv.text = lessonStrTitle

        holder.itemView.setOnClickListener {
            val lessonNumber = position + 1
            val lessonTitle = lessonTitle[position]

            val intent = Intent(activity, WordsSliderActivity::class.java)
            intent.putExtra("course", "tofel")
            intent.putExtra("lesson_number", lessonNumber)
            intent.putExtra("lesson_title", lessonTitle)
            activity.startActivity(intent)
        }
    }

    override fun getItemCount(): Int = lessonTitle.size

    inner class RecyclerTofelItemViewHolder(val bind: LessonsTofelItemsBinding): RecyclerView.ViewHolder(bind.root)
}