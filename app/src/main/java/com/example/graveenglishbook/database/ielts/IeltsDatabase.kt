package com.example.graveenglishbook.database.ielts

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.graveenglishbook.model.IeltsEntity

@Database(entities = [IeltsEntity::class], version = 1)
abstract class IeltsDatabase: RoomDatabase() {

    abstract fun getIeltsDao(): IeltsDao

    companion object {

        @Volatile
        private var instance: IeltsDatabase? = null
        private val LOCK = Any()

        operator fun invoke(context: Context) = instance ?: synchronized(LOCK) {
            instance ?: createDatabase(context).also { instance = it }
        }

        private fun createDatabase(context: Context) =
            Room.databaseBuilder(
                context.applicationContext,
                IeltsDatabase::class.java,
                "dbIelts.db"
            )
                .createFromAsset("databases/dbIelts.db")
                .build()
    }
}