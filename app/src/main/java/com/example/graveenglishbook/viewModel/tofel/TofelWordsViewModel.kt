package com.example.graveenglishbook.viewModel.tofel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.graveenglishbook.repositories.TofelRepository
import kotlinx.coroutines.launch

class TofelWordsViewModel(
    private val repository: TofelRepository

): ViewModel() {

    fun getTofelWords(lessonNumber: Int) = repository.getTofelWords(lessonNumber)

    fun addToFavorite(wordId: Int) = viewModelScope.launch {
        repository.addToFavorite(wordId)
    }

    fun removeFromFavorite(wordId: Int) = viewModelScope.launch {
        repository.removeFromFavorite(wordId)
    }

    suspend fun isFavorite(wordId: Int) = repository.isFavorite(wordId)

    fun getFavoriteWords() = repository.getTofelFavoriteWords()

    fun clearFavoriteWords() = viewModelScope.launch {
        repository.clearTofelFavorite()
    }
}