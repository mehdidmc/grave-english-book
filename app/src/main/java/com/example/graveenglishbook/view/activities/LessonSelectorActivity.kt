package com.example.graveenglishbook.view.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.graveenglishbook.R
import com.example.graveenglishbook.databinding.ActivityLessonSelectorBinding
import com.example.graveenglishbook.view.adapters.RecyclerFiveoFiveItemAdapter
import com.example.graveenglishbook.view.adapters.RecyclerIeltsItemsAdapter
import com.example.graveenglishbook.view.adapters.RecyclerTofelItemsAdapter

class LessonSelectorActivity : AppCompatActivity() {

    private lateinit var bind: ActivityLessonSelectorBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lesson_selector)
        bind = DataBindingUtil.setContentView(this, R.layout.activity_lesson_selector)

        setupRecyclerViewAndTitle()

        bind.backImv.setOnClickListener { onBackPressed() }
    }

    private fun setupRecyclerViewAndTitle() {
        val course = intent.extras?.getString("course")

        when {
            course.equals("504_course") -> {
                bind.titleTxv.text = getString(R.string.words_504_fiveoFive)
                bind.recyclerView.adapter = RecyclerFiveoFiveItemAdapter(this)
            }
            course.equals("tofel_course") -> {
                bind.titleTxv.text = getString(R.string.words_tofel_lesson_title)
                bind.recyclerView.adapter = RecyclerTofelItemsAdapter(this)
            }
            course.equals("ielts_course") -> {
                bind.titleTxv.text = getString(R.string.words_ielts_lesson_title)
                bind.recyclerView.adapter = RecyclerIeltsItemsAdapter(this)
            }
        }

        bind.recyclerView.layoutManager = LinearLayoutManager(this)
    }
}