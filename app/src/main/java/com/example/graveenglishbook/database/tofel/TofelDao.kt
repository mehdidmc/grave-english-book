package com.example.graveenglishbook.database.tofel

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import com.example.graveenglishbook.model.TofelEntity

@Dao
interface TofelDao {

    @Query("SELECT * FROM tbl_tofel WHERE lessonNumber=:lessonNumber")
    fun getTofelWords(lessonNumber: Int): LiveData<List<TofelEntity>>

    @Query("SELECT * FROM tbl_tofel WHERE favorite=1")
    fun getTofelFavoriteWords(): LiveData<List<TofelEntity>>

    @Query("UPDATE tbl_tofel SET favorite=1 WHERE id=:wordId")
    suspend fun addToFavorite(wordId: Int)

    @Query("UPDATE tbl_tofel SET favorite=0 WHERE id=:wordId")
    suspend fun removeFromFavorite(wordId: Int)

    @Query("SELECT favorite FROM tbl_tofel WHERE id=:wordId")
    suspend fun isFavorite(wordId: Int): Int

    @Query("UPDATE tbl_tofel SET favorite=0")
    suspend fun clearFavoriteWords()
}