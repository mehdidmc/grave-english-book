package com.example.graveenglishbook.viewModel.fiveoFive

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.graveenglishbook.repositories.FiveoFiveRepository
import kotlinx.coroutines.launch

class FiveoFiveWordsViewModel(
    private val repository: FiveoFiveRepository
): ViewModel() {

    fun get504Words(lessonNumber: Int) = repository.fiveoFiveWords(lessonNumber)

    fun addTo504Favorites(wordId: Int) = viewModelScope.launch {
        repository.addToFavorites(wordId)
    }

    suspend fun is504Favorite(wordId: Int) = repository.isFavorite(wordId)

    fun getFavoriteWords() = repository.getFavoriteWords()

    fun removeFromFavorites(wordId: Int) = viewModelScope.launch {
        repository.removeFromFavorite(wordId)
    }

    fun clearFavorites() = viewModelScope.launch {
        repository.clearFavorites()
    }

    suspend fun isRead(wordId: Int) =
        repository.isRead(wordId)

    fun setRead(wordId: Int) = viewModelScope.launch {
        repository.setRead(wordId)
    }
}