package com.example.graveenglishbook.database.word504

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.graveenglishbook.model.FiveoFiveEntity

@Database(entities = [FiveoFiveEntity::class], version = 1)
abstract class FiveoFiveDB: RoomDatabase() {

    abstract fun getFiveoFiveDao(): FiveoFiveDao

    companion object {

        @Volatile
        private var instance: FiveoFiveDB? = null
        private var LOCK = Any()

        operator fun invoke(context: Context) = instance ?: synchronized(LOCK) {
            instance ?: createDatabase(context).also { instance = it }
        }

        private fun createDatabase(context: Context) =
            Room.databaseBuilder(
                context.applicationContext,
                FiveoFiveDB::class.java,
                "db504words.db"
            )
                .createFromAsset("databases/db504words.db")
                .build()
    }
}