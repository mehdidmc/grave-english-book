package com.example.graveenglishbook.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey

@Entity(tableName = "tbl_tofel")
data class TofelEntity(
    @ColumnInfo(name = "id")
    @PrimaryKey
    val id: Int,

    @ColumnInfo(name = "words")
    val word: String,

    @ColumnInfo(name = "meaning")
    val meaning: String,

    @ColumnInfo(name = "pronun")
    val pronun: String,

    @ColumnInfo(name = "example")
    val example: String,

    @ColumnInfo(name = "exampleMeaning")
    val exampleMeaning: String,

    @ColumnInfo(name = "example2")
    val example2: String,

    @ColumnInfo(name = "exampleMeaning2")
    val exampleMeaning2: String,

    @ColumnInfo(name = "lessonNumber")
    val lessonNumber: Int,

    @ColumnInfo(name = "favorite")
    val favorite: Int
) {
    @Ignore
    var isExpanded = false
}
