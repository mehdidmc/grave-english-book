package com.example.graveenglishbook.view.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.graveenglishbook.R
import com.example.graveenglishbook.databinding.RecyclerFavoriteTofelWordsItemsBinding
import com.example.graveenglishbook.model.TofelEntity
import com.example.graveenglishbook.viewModel.tofel.TofelWordsViewModel
import com.google.android.material.snackbar.Snackbar

class RecyclerTofelFavoriteAdapter(
    private val viewModel: TofelWordsViewModel

): RecyclerView.Adapter<RecyclerTofelFavoriteAdapter.RecyclerFavoriteTofelViewHolder>() {

    val differ = AsyncListDiffer(this, RecyclerFavoriteTofelDiffUtil())

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerFavoriteTofelViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val bind: RecyclerFavoriteTofelWordsItemsBinding = DataBindingUtil.inflate(inflater, R.layout.recycler_favorite_tofel_words_items, parent, false)
        return RecyclerFavoriteTofelViewHolder(bind)
    }

    override fun onBindViewHolder(holder: RecyclerFavoriteTofelViewHolder, position: Int) {
        val currentWord = differ.currentList[position]
        holder.bind.words = currentWord

        if (currentWord.isExpanded) holder.bind.expandableFavoriteLayout.visibility = View.VISIBLE
        else holder.bind.expandableFavoriteLayout.visibility = View.GONE

        holder.bind.removeFavoriteTxv.setOnClickListener {
            viewModel.removeFromFavorite(currentWord.id)
            Snackbar.make(it, it.context.getString(R.string.remove_Favorite), Snackbar.LENGTH_SHORT).show()
        }
    }

    override fun getItemCount(): Int = differ.currentList.size

    inner class RecyclerFavoriteTofelViewHolder(val bind: RecyclerFavoriteTofelWordsItemsBinding): RecyclerView.ViewHolder(bind.root) {
        init {
            bind.cardView.setOnClickListener {
                val word = differ.currentList[adapterPosition]
                word.isExpanded = !word.isExpanded
                notifyItemChanged(adapterPosition)
            }
        }
    }

    inner class RecyclerFavoriteTofelDiffUtil: DiffUtil.ItemCallback<TofelEntity>() {

        override fun areItemsTheSame(oldItem: TofelEntity, newItem: TofelEntity): Boolean =
            oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: TofelEntity, newItem: TofelEntity): Boolean =
            oldItem == newItem
    }
}