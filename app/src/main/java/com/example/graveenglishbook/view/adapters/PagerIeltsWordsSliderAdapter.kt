package com.example.graveenglishbook.view.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.viewModelScope
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.graveenglishbook.R
import com.example.graveenglishbook.databinding.WordsIeltsPagerItemBinding
import com.example.graveenglishbook.model.IeltsEntity
import com.example.graveenglishbook.viewModel.ielts.IeltsWordsSliderViewModel
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.launch

class PagerIeltsWordsSliderAdapter(
    private val context: Context,
    private val viewModel: IeltsWordsSliderViewModel

): RecyclerView.Adapter<PagerIeltsWordsSliderAdapter.PagerIeltsWordsSliderViewHolder>() {

    val differ = AsyncListDiffer(this, PagerIeltsWordsSliderDifUtil())

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PagerIeltsWordsSliderViewHolder {
        val inflater = LayoutInflater.from(context)
        val bind: WordsIeltsPagerItemBinding = DataBindingUtil.inflate(inflater, R.layout.words_ielts_pager_item, parent, false)
        return PagerIeltsWordsSliderViewHolder(bind)
    }

    override fun onBindViewHolder(holder: PagerIeltsWordsSliderViewHolder, position: Int) {
        val ieltsWord = differ.currentList[position]
        holder.bind.ieltsWord = ieltsWord

        viewModel.viewModelScope.launch {
            if (viewModel.isFavorite(ieltsWord.id) == 1) {
                holder.bind.addFavoriteTxv.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.heart_minus,0,0,0)
                holder.bind.addFavoriteTxv.text = context.getString(R.string.remove_Favorite)

            } else {
                holder.bind.addFavoriteTxv.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.heart_plus,0,0,0)
                holder.bind.addFavoriteTxv.text = context.getString(R.string.add_favorite_txv)
            }
        }

        holder.bind.addFavoriteTxv.setOnClickListener {
            if (holder.bind.addFavoriteTxv.text == context.getString(R.string.remove_Favorite)) {
                viewModel.removeFromFavorite(ieltsWord.id)
                holder.bind.addFavoriteTxv.text = context.getString(R.string.add_favorite_txv)
                holder.bind.addFavoriteTxv.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.heart_plus,0,0,0)
                Snackbar.make(it, context.getString(R.string.toast_remove_favorite), Snackbar.LENGTH_SHORT).show()
            }

            else {
                viewModel.addToFavorite(ieltsWord.id)
                holder.bind.addFavoriteTxv.text = context.getString(R.string.remove_Favorite)
                holder.bind.addFavoriteTxv.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.heart_minus,0,0,0)
                Snackbar.make(it, context.getString(R.string.toast_add_favorite), Snackbar.LENGTH_SHORT).show()
            }
        }
    }

    override fun getItemCount(): Int = differ.currentList.size

    inner class PagerIeltsWordsSliderViewHolder(val bind: WordsIeltsPagerItemBinding): RecyclerView.ViewHolder(bind.root)

    inner class PagerIeltsWordsSliderDifUtil: DiffUtil.ItemCallback<IeltsEntity>() {

        override fun areItemsTheSame(oldItem: IeltsEntity, newItem: IeltsEntity): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: IeltsEntity, newItem: IeltsEntity): Boolean {
            return oldItem == newItem
        }
    }
}