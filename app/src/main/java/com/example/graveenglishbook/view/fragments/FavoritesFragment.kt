package com.example.graveenglishbook.view.fragments

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.example.graveenglishbook.R
import com.example.graveenglishbook.databinding.FragmentFavoritesBinding
import com.example.graveenglishbook.view.activities.FavoriteWordsActivity

class FavoritesFragment : Fragment() {

    private lateinit var bind: FragmentFavoritesBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        bind = DataBindingUtil.inflate(inflater, R.layout.fragment_favorites, container, false)

        bind.card504.setOnClickListener {
            val intent = Intent(activity, FavoriteWordsActivity::class.java)
            intent.putExtra("favorite", "504")
            startActivity(intent)
        }

        bind.cardTofel.setOnClickListener {
            val intent = Intent(activity, FavoriteWordsActivity::class.java)
            intent.putExtra("favorite", "tofel")
            startActivity(intent)
        }

        bind.cardIelts.setOnClickListener {
            val intent = Intent(activity, FavoriteWordsActivity::class.java)
            intent.putExtra("favorite", "ielts")
            startActivity(intent)
        }

        return bind.root
    }
}