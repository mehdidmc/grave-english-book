package com.example.graveenglishbook.view.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.graveenglishbook.R
import com.example.graveenglishbook.databinding.RecyclerFavoriteIeltsWordsBinding
import com.example.graveenglishbook.model.IeltsEntity
import com.example.graveenglishbook.viewModel.fiveoFive.FiveoFiveWordsViewModel
import com.example.graveenglishbook.viewModel.ielts.IeltsWordsSliderViewModel
import com.google.android.material.snackbar.Snackbar

class RecyclerIeltsFavoritesAdapter(
    private val viewModel: IeltsWordsSliderViewModel

): RecyclerView.Adapter<RecyclerIeltsFavoritesAdapter.RecyclerIeltsFavoritesViewHolder>() {

    val differ = AsyncListDiffer(this, RecyclerIeltsFavoriteDiffUtil())

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerIeltsFavoritesViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val bind: RecyclerFavoriteIeltsWordsBinding = DataBindingUtil.inflate(inflater, R.layout.recycler_favorite_ielts_words, parent, false)
        return RecyclerIeltsFavoritesViewHolder(bind)
    }

    override fun onBindViewHolder(holder: RecyclerIeltsFavoritesViewHolder, position: Int) {
        val ielts = differ.currentList[position]
        holder.bind.ieltsWord = ielts

        if (ielts.isExpanded) holder.bind.expandableFavoriteLayout.visibility = View.VISIBLE
        else holder.bind.expandableFavoriteLayout.visibility = View.GONE

        holder.bind.removeFavoriteTxv.setOnClickListener {
            viewModel.removeFromFavorite(ielts.id)
            Snackbar.make(it, it.context.getString(R.string.remove_Favorite), Snackbar.LENGTH_SHORT).show()
        }
    }

    override fun getItemCount(): Int = differ.currentList.size

    inner class RecyclerIeltsFavoritesViewHolder(val bind: RecyclerFavoriteIeltsWordsBinding): RecyclerView.ViewHolder(bind.root) {
        init {
            bind.cardLayout.setOnClickListener {
                val word = differ.currentList[adapterPosition]
                word.isExpanded = !word.isExpanded
                notifyItemChanged(adapterPosition)
            }
        }
    }

    inner class RecyclerIeltsFavoriteDiffUtil: DiffUtil.ItemCallback<IeltsEntity>() {

        override fun areItemsTheSame(oldItem: IeltsEntity, newItem: IeltsEntity): Boolean =
            oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: IeltsEntity, newItem: IeltsEntity): Boolean =
            oldItem == newItem
    }
}