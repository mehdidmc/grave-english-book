package com.example.graveenglishbook.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey

@Entity(tableName = "tbl_ielts")
data class IeltsEntity(

    @ColumnInfo(name = "id")
    @PrimaryKey
    val id: Int,

    @ColumnInfo(name = "words")
    val word: String,

    @ColumnInfo(name = "pronun")
    val pronun: String,

    @ColumnInfo(name = "persianWords")
    val meaning: String,

    @ColumnInfo(name = "definition")
    val definition: String,

    @ColumnInfo(name = "synonym")
    val synonym: String,

    @ColumnInfo(name = "antonym")
    val antonym: String,

    @ColumnInfo(name = "lessonNumber")
    val lessonNumber: Int,

    @ColumnInfo(name = "favorite")
    val favorite: Int
) {
    @Ignore
    var isExpanded = false
}
