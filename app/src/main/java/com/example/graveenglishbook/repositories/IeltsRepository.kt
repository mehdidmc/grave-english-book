package com.example.graveenglishbook.repositories

import com.example.graveenglishbook.database.ielts.IeltsDatabase

class IeltsRepository(private val db: IeltsDatabase) {

    fun getIeltsWords(lessonNumber: Int) = db.getIeltsDao().getIeltsWords(lessonNumber)

    fun getIeltsFavoriteWords() = db.getIeltsDao().getIeltsFavoriteWords()

    suspend fun addToFavorite(wordId: Int) = db.getIeltsDao().addToFavorite(wordId)

    suspend fun isFavorite(wordId: Int) = db.getIeltsDao().isFavorite(wordId)

    suspend fun removeFromFavorite(wordId: Int) = db.getIeltsDao().removeFromFavorite(wordId)

    suspend fun clearFavorites() = db.getIeltsDao().clearFavoriteWords()
}