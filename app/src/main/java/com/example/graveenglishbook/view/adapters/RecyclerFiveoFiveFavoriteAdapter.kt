package com.example.graveenglishbook.view.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.graveenglishbook.R
import com.example.graveenglishbook.databinding.RecyclerFavorite504WordsItemBinding
import com.example.graveenglishbook.model.FiveoFiveEntity
import com.example.graveenglishbook.viewModel.fiveoFive.FiveoFiveWordsViewModel
import com.google.android.material.snackbar.Snackbar

class RecyclerFiveoFiveFavoriteAdapter(
    private val viewModel504: FiveoFiveWordsViewModel

): RecyclerView.Adapter<RecyclerFiveoFiveFavoriteAdapter.RecyclerFiveoFiveFavoriteVH>() {

    val differ = AsyncListDiffer(this, RecyclerFavoriteDiffUtil())

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerFiveoFiveFavoriteVH {
        val inflater = LayoutInflater.from(parent.context)
        val bind: RecyclerFavorite504WordsItemBinding = DataBindingUtil.inflate(inflater, R.layout.recycler_favorite_504_words_item, parent, false)
        return RecyclerFiveoFiveFavoriteVH(bind)
    }

    override fun onBindViewHolder(holder: RecyclerFiveoFiveFavoriteVH, position: Int) {
        val currentWord = differ.currentList[position]
        holder.bind.word = currentWord

        if (currentWord.isExpanded) holder.bind.expandableFavoriteLayout.visibility = View.VISIBLE
        else holder.bind.expandableFavoriteLayout.visibility = View.GONE

        holder.bind.removeFavoriteTxv.setOnClickListener {
            viewModel504.removeFromFavorites(currentWord.id)
            Snackbar.make(it, it.context.getString(R.string.remove_Favorite), Snackbar.LENGTH_SHORT).show()
        }
    }

    fun setData(newData: List<FiveoFiveEntity>) {
        differ.submitList(newData)
    }

    override fun getItemCount(): Int = differ.currentList.size

    inner class RecyclerFiveoFiveFavoriteVH(val bind: RecyclerFavorite504WordsItemBinding): RecyclerView.ViewHolder(bind.root) {

        init {
            bind.cardView.setOnClickListener {
                val word = differ.currentList[adapterPosition]
                word.isExpanded = !word.isExpanded
                notifyItemChanged(adapterPosition)
            }
        }
    }

    inner class RecyclerFavoriteDiffUtil: DiffUtil.ItemCallback<FiveoFiveEntity>() {

        override fun areItemsTheSame(oldItem: FiveoFiveEntity, newItem: FiveoFiveEntity): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: FiveoFiveEntity, newItem: FiveoFiveEntity): Boolean {
            return oldItem == newItem
        }
    }
}