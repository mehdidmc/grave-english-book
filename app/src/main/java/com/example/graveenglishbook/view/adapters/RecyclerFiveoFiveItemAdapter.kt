package com.example.graveenglishbook.view.adapters

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.graveenglishbook.R
import com.example.graveenglishbook.databinding.Lessons504ItemsBinding
import com.example.graveenglishbook.view.activities.WordsSliderActivity

class RecyclerFiveoFiveItemAdapter(
    private val activity: Activity,

): RecyclerView.Adapter<RecyclerFiveoFiveItemAdapter.RecyclerFiveoFiveViewHolder>() {

    private val lessonTitles = activity.resources.getStringArray(R.array.lesson_titles_504)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerFiveoFiveViewHolder {
        val inflater = LayoutInflater.from(activity)
        val bind: Lessons504ItemsBinding = DataBindingUtil.inflate(inflater, R.layout.lessons_504_items, parent, false)
        return RecyclerFiveoFiveViewHolder(bind)
    }

    override fun onBindViewHolder(holder: RecyclerFiveoFiveViewHolder, position: Int) {
        val lessonStrTitle = "LESSON ${lessonTitles[position]}"
        holder.bind.lessonTitleTxv.text = lessonStrTitle
        holder.bind.wordsCountTxv.text = activity.resources.getString(R.string.words_504_counts)

        holder.itemView.setOnClickListener {
            val lessonNumber = position + 1
            val lessonTitle = lessonTitles[position]

            val intent = Intent(activity, WordsSliderActivity::class.java)
            intent.putExtra("lesson_number", lessonNumber)
            intent.putExtra("lesson_title", lessonTitle)
            intent.putExtra("course", "504")
            activity.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return lessonTitles.size
    }

    // ViewHolder
    inner class RecyclerFiveoFiveViewHolder(val bind: Lessons504ItemsBinding): RecyclerView.ViewHolder(bind.root)
}