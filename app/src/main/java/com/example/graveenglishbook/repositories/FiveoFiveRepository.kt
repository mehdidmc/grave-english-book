package com.example.graveenglishbook.repositories

import com.example.graveenglishbook.database.word504.FiveoFiveDB

class FiveoFiveRepository(private val db: FiveoFiveDB) {

    fun fiveoFiveWords(lessonNumber: Int) =
        db.getFiveoFiveDao().getWords(lessonNumber)

    suspend fun addToFavorites(wordId: Int) =
        db.getFiveoFiveDao().addToFavorites(wordId)

    suspend fun removeFromFavorite(wordId: Int) =
        db.getFiveoFiveDao().removeFromFavorite(wordId)

    suspend fun isFavorite(wordId: Int) =
        db.getFiveoFiveDao().isFavorite(wordId)

    suspend fun clearFavorites() = db.getFiveoFiveDao().clearFavorites()

    fun getFavoriteWords() =
        db.getFiveoFiveDao().getFavoriteWords()

    suspend fun isRead(wordId: Int) =
        db.getFiveoFiveDao().isRead(wordId)

    suspend fun setRead(wordId: Int) =
        db.getFiveoFiveDao().setRead(wordId)
}