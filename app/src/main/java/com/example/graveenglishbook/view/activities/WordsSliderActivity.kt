package com.example.graveenglishbook.view.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.speech.tts.TextToSpeech
import android.util.Log
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.CompositePageTransformer
import androidx.viewpager2.widget.MarginPageTransformer
import androidx.viewpager2.widget.ViewPager2
import com.example.graveenglishbook.R
import com.example.graveenglishbook.database.ielts.IeltsDatabase
import com.example.graveenglishbook.database.tofel.TofelDatabase
import com.example.graveenglishbook.database.word504.FiveoFiveDB
import com.example.graveenglishbook.databinding.ActivityWordsSliderBinding
import com.example.graveenglishbook.repositories.FiveoFiveRepository
import com.example.graveenglishbook.repositories.IeltsRepository
import com.example.graveenglishbook.repositories.TofelRepository
import com.example.graveenglishbook.view.adapters.Pager504WordsSliderAdapter
import com.example.graveenglishbook.view.adapters.PagerIeltsWordsSliderAdapter
import com.example.graveenglishbook.view.adapters.PagerTofelWordsSliderAdapter
import com.example.graveenglishbook.viewModel.fiveoFive.FiveoFiveWordsViewModel
import com.example.graveenglishbook.viewModel.fiveoFive.FiveoFiveWordsViewModelFactory
import com.example.graveenglishbook.viewModel.ielts.IeltsWordsSliderViewModel
import com.example.graveenglishbook.viewModel.ielts.IeltsWordsSliderViewModelFactory
import com.example.graveenglishbook.viewModel.tofel.TofelWordsViewModel
import com.example.graveenglishbook.viewModel.tofel.TofelWordsViewModelFactory
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.util.*
import kotlin.math.abs

class WordsSliderActivity : AppCompatActivity() {

    private lateinit var bind: ActivityWordsSliderBinding
    private lateinit var speech: TextToSpeech

    private lateinit var viewModelFiveoFive: FiveoFiveWordsViewModel
    private lateinit var viewModelTofel: TofelWordsViewModel
    private lateinit var viewModelIelts: IeltsWordsSliderViewModel

    private lateinit var pagerAdapter504: Pager504WordsSliderAdapter
    private lateinit var pagerAdapterTofel: PagerTofelWordsSliderAdapter
    private lateinit var pagerAdapterIelts: PagerIeltsWordsSliderAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_words_slider)
        bind = DataBindingUtil.setContentView(this, R.layout.activity_words_slider)

        setupLessonTitle()
        setupTextToSpeech()
        initializing()
        setupViewPager()
        fetchData()
        setItemCounts()
        isWordRead()

        bind.pronunciation.setOnClickListener {
            var word = "Nothing"

            when {
                course().equals("504") -> word = pagerAdapter504.differ.currentList[bind.viewPager.currentItem].word
                course().equals("tofel") -> word = pagerAdapterTofel.differ.currentList[bind.viewPager.currentItem].word
                course().equals("ielts") -> word = pagerAdapterIelts.differ.currentList[bind.viewPager.currentItem].word
            }

            speech.speak(word, TextToSpeech.QUEUE_FLUSH, null, "Nothing")
        }

        bind.backImv.setOnClickListener { onBackPressed() }
    }

    private fun setupLessonTitle() {
        val lessonTitle = intent.extras?.getString("lesson_title")
        bind.lessonTitle = "LESSON $lessonTitle"

        when {
            course().equals("504") -> bind.titleTxv.text = getString(R.string.words_504)
            course().equals("tofel") -> bind.titleTxv.text = getString(R.string.tofel_words)
            course().equals("ielts") -> bind.titleTxv.text = getString(R.string.ielts_words)
        }
    }

    private fun setupTextToSpeech() {
        speech = TextToSpeech(this) {
            if (it == TextToSpeech.SUCCESS) {
                val result = speech.setLanguage(Locale.ENGLISH)
                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED)
                    Log.d("TextToSpeech", "setupSpeech: Language not Supported")
            } else
                Log.d("TextToSpeech", "setupSpeech: Initialization Failed")
        }
    }

    private fun initializing() {
        when {
            course().equals("504") -> {
                val repository = FiveoFiveRepository(FiveoFiveDB(this))
                val provider = FiveoFiveWordsViewModelFactory(repository)
                viewModelFiveoFive = ViewModelProvider(this, provider)[FiveoFiveWordsViewModel::class.java]
                pagerAdapter504 = Pager504WordsSliderAdapter(this, viewModelFiveoFive)
            }
            course().equals("tofel") -> {
                val repository = TofelRepository(TofelDatabase(this))
                val provider = TofelWordsViewModelFactory(repository)
                viewModelTofel = ViewModelProvider(this, provider)[TofelWordsViewModel::class.java]
                pagerAdapterTofel = PagerTofelWordsSliderAdapter(this, viewModelTofel)
            }
            course().equals("ielts") -> {
                val repository = IeltsRepository(IeltsDatabase(this))
                val provider = IeltsWordsSliderViewModelFactory(repository)
                viewModelIelts = ViewModelProvider(this, provider)[IeltsWordsSliderViewModel::class.java]
                pagerAdapterIelts = PagerIeltsWordsSliderAdapter(this, viewModelIelts)
            }
        }
    }

    private fun setupViewPager() {
        val pageTransfer = CompositePageTransformer()
        pageTransfer.addTransformer(MarginPageTransformer(10))
        pageTransfer.addTransformer { page, position ->
            val r: Float = 1 - abs(position)
            page.scaleY = 0.85f + r * 0.15f
        }

        when {
            course().equals("504") -> bind.viewPager.adapter = pagerAdapter504
            course().equals("tofel") -> bind.viewPager.adapter = pagerAdapterTofel
            course().equals("ielts") -> bind.viewPager.adapter = pagerAdapterIelts
        }

        bind.viewPager.apply {
            clipToPadding = false
            clipChildren = false
            offscreenPageLimit = 3
            getChildAt(0).overScrollMode = RecyclerView.OVER_SCROLL_NEVER
            setPageTransformer(pageTransfer)
        }
    }

    private fun fetchData() {
        val lessonNumber = intent.extras?.getInt("lesson_number")

        when {
            course().equals("504") -> {
                viewModelFiveoFive.get504Words(lessonNumber!!).observe(this) {
                    pagerAdapter504.differ.submitList(it)
                }
            }

            course().equals("tofel") -> {
                viewModelTofel.getTofelWords(lessonNumber!!).observe(this) {
                    pagerAdapterTofel.differ.submitList(it)
                }
            }

            course().equals("ielts") -> {
                viewModelIelts.getIeltsWords(lessonNumber!!).observe(this) { item ->
                    pagerAdapterIelts.differ.submitList(item)
                }
            }
        }
    }

    private fun isWordRead() {
        bind.viewPager.registerOnPageChangeCallback(object: ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                when {
                    course().equals("504") -> {
                        val wordId = pagerAdapter504.differ.currentList[position].id
                        lifecycleScope.launch {
                            if (viewModelFiveoFive.isRead(wordId) == 1) {
                                pagerAdapter504.setReadVisibility(View.VISIBLE)

                            } else {
                                delay(2000)
                                viewModelFiveoFive.setRead(wordId)
                                pagerAdapter504.setReadVisibility(View.VISIBLE)
                            }
                        }
                    }
                }
            }
        })
    }

    private fun setItemCounts() {
        bind.viewPager.registerOnPageChangeCallback(object: ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                var countItem = "0 / 0"

                when {
                    course().equals("504") -> countItem = "${position + 1} / ${pagerAdapter504.differ.currentList.size}"
                    course().equals("tofel") -> countItem = "${position + 1} / ${pagerAdapterTofel.differ.currentList.size}"
                    course().equals("ielts") -> countItem = "${position + 1} / ${pagerAdapterIelts.differ.currentList.size}"
                }

                bind.wordCountsTxv.text = countItem
            }
        })
    }

    private fun course() = intent.extras?.getString("course")

    override fun onDestroy() {
        super.onDestroy()
        speech.stop()
        speech.shutdown()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        speech.stop()
        speech.shutdown()
    }
}