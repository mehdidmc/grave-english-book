package com.example.graveenglishbook.repositories

import com.example.graveenglishbook.database.tofel.TofelDatabase

class TofelRepository(private val db: TofelDatabase) {

    fun getTofelWords(lessonNumber: Int) = db.getTofelDao().getTofelWords(lessonNumber)

    fun getTofelFavoriteWords() = db.getTofelDao().getTofelFavoriteWords()

    suspend fun addToFavorite(wordId: Int) = db.getTofelDao().addToFavorite(wordId)

    suspend fun isFavorite(wordId: Int) = db.getTofelDao().isFavorite(wordId)

    suspend fun removeFromFavorite(wordId: Int) = db.getTofelDao().removeFromFavorite(wordId)

    suspend fun clearTofelFavorite() = db.getTofelDao().clearFavoriteWords()
}