package com.example.graveenglishbook.view.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.graveenglishbook.R
import com.example.graveenglishbook.database.ielts.IeltsDatabase
import com.example.graveenglishbook.database.tofel.TofelDatabase
import com.example.graveenglishbook.database.word504.FiveoFiveDB
import com.example.graveenglishbook.databinding.ActivityFavoriteWordsBinding
import com.example.graveenglishbook.repositories.FiveoFiveRepository
import com.example.graveenglishbook.repositories.IeltsRepository
import com.example.graveenglishbook.repositories.TofelRepository
import com.example.graveenglishbook.view.adapters.RecyclerFiveoFiveFavoriteAdapter
import com.example.graveenglishbook.view.adapters.RecyclerIeltsFavoritesAdapter
import com.example.graveenglishbook.view.adapters.RecyclerTofelFavoriteAdapter
import com.example.graveenglishbook.viewModel.fiveoFive.FiveoFiveWordsViewModel
import com.example.graveenglishbook.viewModel.fiveoFive.FiveoFiveWordsViewModelFactory
import com.example.graveenglishbook.viewModel.ielts.IeltsWordsSliderViewModel
import com.example.graveenglishbook.viewModel.ielts.IeltsWordsSliderViewModelFactory
import com.example.graveenglishbook.viewModel.tofel.TofelWordsViewModel
import com.example.graveenglishbook.viewModel.tofel.TofelWordsViewModelFactory

class FavoriteWordsActivity : AppCompatActivity() {

    private lateinit var bind: ActivityFavoriteWordsBinding

    private lateinit var recycler504Adapter: RecyclerFiveoFiveFavoriteAdapter
    private lateinit var recyclerTofelAdapter: RecyclerTofelFavoriteAdapter
    private lateinit var recyclerIeltsAdapter: RecyclerIeltsFavoritesAdapter

    private lateinit var viewModel504: FiveoFiveWordsViewModel
    private lateinit var viewModelTofel: TofelWordsViewModel
    private lateinit var viewModelIelts: IeltsWordsSliderViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_favorite_words)
        bind = DataBindingUtil.setContentView(this, R.layout.activity_favorite_words)

        setTitleName()
        setupViewModel()
        setupRecyclerView()
        getFavoriteWords()

        bind.backImv.setOnClickListener { finish() }

        bind.clearFavoritesTxv.setOnClickListener {
            when {
                course().equals("504") -> viewModel504.clearFavorites()
                course().equals("tofel") -> viewModelTofel.clearFavoriteWords()
                course().equals("ielts") -> viewModelIelts.clearAllFavorites()
            }
        }
    }

    private fun setTitleName() {
        when {
            course().equals("504") -> bind.titleTxv.text = getString(R.string.favorite_504_words)
            course().equals("tofel") -> bind.titleTxv.text = getString(R.string.favorite_tofel_words)
            course().equals("ielts") -> bind.titleTxv.text = getString(R.string.favorite_ielts_words)
        }
    }

    private fun setupViewModel() {
        when {
            course().equals("504") -> {
                val repository = FiveoFiveRepository(FiveoFiveDB.invoke(this))
                val provider = FiveoFiveWordsViewModelFactory(repository)
                viewModel504 = ViewModelProvider(this, provider)[FiveoFiveWordsViewModel::class.java]

            }
            course().equals("tofel") -> {
                val repository = TofelRepository(TofelDatabase.invoke(this))
                val provider = TofelWordsViewModelFactory(repository)
                viewModelTofel = ViewModelProvider(this, provider)[TofelWordsViewModel::class.java]

            }
            course().equals("ielts") -> {
                val repository = IeltsRepository(IeltsDatabase(this))
                val provider = IeltsWordsSliderViewModelFactory(repository)
                viewModelIelts = ViewModelProvider(this, provider)[IeltsWordsSliderViewModel::class.java]
            }
        }
    }

    private fun setupRecyclerView() {
        when {
            course().equals("504") -> {
                recycler504Adapter = RecyclerFiveoFiveFavoriteAdapter(viewModel504)
                bind.recyclerView.adapter = recycler504Adapter

            }
            course().equals("tofel") -> {
                recyclerTofelAdapter = RecyclerTofelFavoriteAdapter(viewModelTofel)
                bind.recyclerView.adapter = recyclerTofelAdapter

            }
            course().equals("ielts") -> {
                recyclerIeltsAdapter = RecyclerIeltsFavoritesAdapter(viewModelIelts)
                bind.recyclerView.adapter = recyclerIeltsAdapter
            }
        }

        bind.recyclerView.layoutManager = LinearLayoutManager(this)
    }

    private fun getFavoriteWords() {
        when {
            course().equals("504") -> {
                viewModel504.getFavoriteWords().observe(this) {
                    recycler504Adapter.setData(it)
                    if (it.isEmpty()) {
                        bind.noDataLayout.visibility = View.VISIBLE
                        bind.clearFavoritesTxv.visibility = View.GONE
                    }
                }
            }
            course().equals("tofel") -> {
                viewModelTofel.getFavoriteWords().observe(this) {
                    recyclerTofelAdapter.differ.submitList(it)
                    if (it.isEmpty()) {
                        bind.noDataLayout.visibility = View.VISIBLE
                        bind.clearFavoritesTxv.visibility = View.GONE
                    }
                }
            }
            course().equals("ielts") -> {
                viewModelIelts.getIeltsFavorites().observe(this) {
                    recyclerIeltsAdapter.differ.submitList(it)
                    if (it.isEmpty()) {
                        bind.noDataLayout.visibility = View.VISIBLE
                        bind.clearFavoritesTxv.visibility = View.GONE
                    }
                }
            }
        }
    }

    private fun course() = intent.extras?.getString("favorite")
}