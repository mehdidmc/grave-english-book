package com.example.graveenglishbook.view.fragments

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.example.graveenglishbook.R
import com.example.graveenglishbook.databinding.FragmentCoursesBinding
import com.example.graveenglishbook.view.activities.LessonSelectorActivity

class CoursesFragment : Fragment() {

    private lateinit var bind: FragmentCoursesBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        bind = DataBindingUtil.inflate(inflater, R.layout.fragment_courses, container, false)

        bind.layout504.setOnClickListener {
            activity?.let {
                val intent = Intent(it, LessonSelectorActivity::class.java)
                intent.putExtra("course", "504_course")
                startActivity(intent)
            }
        }

        bind.layoutTofel.setOnClickListener {
            activity?.let {
                val intent = Intent(it, LessonSelectorActivity::class.java)
                intent.putExtra("course", "tofel_course")
                startActivity(intent)
            }
        }

        bind.layoutIelts.setOnClickListener {
            activity?.let {
                val intent = Intent(it, LessonSelectorActivity::class.java)
                intent.putExtra("course", "ielts_course")
                startActivity(intent)
            }
        }

        return bind.root
    }
}