package com.example.graveenglishbook.database.tofel

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.graveenglishbook.model.TofelEntity

@Database(entities = [TofelEntity::class], version = 1)
abstract class TofelDatabase: RoomDatabase() {

    abstract fun getTofelDao(): TofelDao

    companion object {

        @Volatile
        private var instance: TofelDatabase? = null
        private var LOCK = Any()

        operator fun invoke(context: Context) = instance ?: synchronized(LOCK) {
            instance ?: createDatabase(context).also { instance = it }
        }

        private fun createDatabase(context: Context) =
            Room.databaseBuilder(
                context.applicationContext,
                TofelDatabase::class.java,
                "dbToefl.db"
            )
                .createFromAsset("databases/dbToefl.db")
                .build()
    }
}